<div class="col-lg-2 col-md-2 col-sm-4">
  <div class="bs-component">
    <h3>Menu</h3>
  </div>
  <div class="list-group table-of-contents">
    <a class="list-group-item {{{ (Request::is('home') ? 'active' : '') }}}" href="{{ url('/home') }}"> Dashboard</a>
    <a class="list-group-item {{{ (Request::is('admin/authors') ? 'active' : '') }}}" href="{{ route('authors.index') }}"> Penulis</a>
    <a class="list-group-item {{{ (Request::is('admin/categories') ? 'active' : '') }}}" href="{{ route('categories.index') }}"> Kategori</a>
    <a class="list-group-item {{{ (Request::is('admin/books') ? 'active' : '') }}}" href="{{ route('books.index') }}"> Buku</a>
    <a class="list-group-item {{{ (Request::is('admin/members') ? 'active' : '') }}}" href="{{ route('members.index') }}"> Member</a>
  </div>
</div>