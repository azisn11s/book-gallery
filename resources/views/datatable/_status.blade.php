{!! Form::model($model, ['url' => $form_url, 'method' => 'post', 'class' => 'form-inline js-confirm', 'data-confirm' => $confirm_message] ) !!}
    {!! Form::submit($button_name, ['class'=>"btn btn-xs $button_type"]) !!}
{!! Form::close()!!}

