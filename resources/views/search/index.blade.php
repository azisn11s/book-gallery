@extends('layouts.app')

@section('content')

	<div class="container">

		<div class="page-header">
	      <h3>Hasil Pencarian <i>{{ $title }}</i></h3>
	    </div>

		<div class="row">
			<div class="col-lg-12">

				<div class="list-group">
					@if($books->count() > 0)
						@for($i=0; $i<$books->count(); $i++)
							<a href="{{ url('/book-detail').'/'.$books[$i]->id }}" class="list-group-item">
								<h4 class="list-group-item-heading">{{ $books[$i]->title }} <small><i> {{ $books[$i]->author->name }}</i></small></h4>
								<p class="list-group-item-text">
									{{ str_limit(strip_tags($books[$i]->description), 100)  }}
								</p>
							</a>
						@endfor
					@else
						<h4 class="text-muted"><i>Tidak ada data yg ditemukan.</i></h4>
					@endif
					
				</div>
			</div>
		</div>

	</div>

@endsection