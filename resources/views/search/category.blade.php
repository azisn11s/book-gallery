@extends('layouts.app')

@section('content')

	<div class="container">
		@if($books->count() > 0)
			<div class="page-header">
		      <h3>{{ $category->name }}</h3>
		    </div>
			<div class="row">
				@php
					$numberOfColumns = 4;
					$rowCount = 0;
					$bootstrapColumnWidth = 12 / $numberOfColumns;
				@endphp
				@for($i=0; $i<$books->count(); $i++)
				<div class="col-lg-{{ $bootstrapColumnWidth }} col-xs-{{ $bootstrapColumnWidth+3 }} cover">
				  <div class="panel panel-default">
				    @php
				      $image_url = asset('img').'/'.$books[$i]->cover;
				      // https://lorempixel.com/640/320/?
				      $image_url = 'https://lorempixel.com/300/400/'.$books[$i]->cover;
				      $image_placeholder = asset('images/book_placeholder_300x400.jpg');
				      if(is_null($books[$i]->cover)) $image_url = $image_placeholder;
				    @endphp
				    <img class="img-responsive card-img-top" src="{{ $image_url }}" alt="">
				    <div class="panel-body">
				      <h4 class="panel-title">{{ $books[$i]->title }}</h4>
				      <h5 class="text-muted">{{ $books[$i]->category->name }}</h5>
				      <p class="card-text"> {{ (!is_null($books[$i]->author)) ? $books[$i]->author->name : '' }} </p>
				    </div>
				    <div class="panel-footer">
				      <a href="{{ url('/book-detail').'/'.$books[$i]->id }}" class="btn btn-primary">Detail</a>
				      @role('member')
				        @if(!is_null($books[$i]->pdf_file) && file_exists(public_path().'/pdf/'.$books[$i]->pdf_file))
				          <a href="{{ url('/read').'/'.$books[$i]->id }}" class="btn btn-primary">Baca</a>
				        @else
				          <a href="#" class="text-muted pull-right non-pdf"> <i>  PDF tidak tersedia</i> </a>
				        @endif
				      
				      @endrole
				    </div>
				  </div>
				</div>
				@php
				  $rowCount++;
				  if($rowCount % $numberOfColumns == 0) echo '</div><div class="row">';
				@endphp
				@endfor
			</div>

		    {{-- Book Pagination --}}
		    <div class="row">
		      <div class="col-lg-12">
		        <center>{{ $books->links('vendor.pagination.default') }}</center>
		      </div>
		    </div>
		@else
			<div class="row">
				<div class="col-lg-12">
					<h4 class="text-muted"><i>Tidak ada data buku.</i></h4>
				</div>
			</div>
		@endif
	</div>
	

@endsection