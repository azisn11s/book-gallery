

Terdapat pesan dari pengunjung DIGILIB.

<table style="border: 0px;">
	<tr>
		<td style="width: 50px;">Nama</td>
		<td><b>{{ $contactUs->name }}</b></td>
	</tr>
	<tr>
		<td style="width: 50px;">Email</td>
		<td><b>{{ $contactUs->email }}</b></td>
	</tr>
	<tr>
		<td style="width: 50px;">Telp/HP</td>
		<td><b>{{ $contactUs->phone }}</b></td>
	</tr>
	<tr>
		<td style="width: 50px;">Subjek</td>
		<td><b>{{ $contactUs->subject }}</b></td>
	</tr>
	<tr>
		<td style="width: 50px;">Pesan</td>
		<td><b>{{ $contactUs->message }}</b></td>
	</tr>
</table>