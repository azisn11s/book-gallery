@extends('layouts.app')

@section('content')
      
      <!-- Carousel -->
      <div id="carousel-example" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example" data-slide-to="1"></li>
          <li data-target="#carousel-example" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner">
          <div class="item active">
            <a href="#"><img src="..//images/banner_digilib_kejaksaan.png" /></a>
          </div>
          <div class="item">
            <a href="#"><img src="..//images/banner_digilib_kejaksaan2.png" /></a>
          </div>
          <div class="item">
            <a href="#"><img src="..//images/banner_digilib_kejaksaan3.png" /></a>
          </div>
        </div>

        <a class="left carousel-control" href="#carousel-example" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>

  <div class="container">

    {{-- Page Header: Book Update --}}
    <div class="page-header">
      <h3>Buku Terbaru</h3>
    </div>

    {{-- Book Update --}}
    <div class="row">
    @if($books->count() > 0)
      @php
        $numberOfColumns = 4;
        $rowCount = 0;
        $bootstrapColumnWidth = 12 / $numberOfColumns;
      @endphp
      
      @for($i=0; $i<$books->count(); $i++)
        <div class="col-lg-{{ $bootstrapColumnWidth }} col-xs-{{ $bootstrapColumnWidth+3 }} cover">
          <div class="panel panel-default">
            @php
              $image_url = asset('img').'/'.$books[$i]->cover;
              // https://lorempixel.com/640/320/?
              // $image_url = 'https://lorempixel.com/300/400/'.$books[$i]->cover;
              $image_placeholder = asset('images/book_placeholder_300x400.jpg');
              if(is_null($books[$i]->cover)) $image_url = $image_placeholder;
            @endphp
            <img class="img-responsive card-img-top" src="{{ $image_url }}" alt="">
            <div class="panel-body">
              <h4 class="panel-title">{{ $books[$i]->title }}</h4>
              <h5 class="text-muted">{{ $books[$i]->category->name }}</h5>
              <p class="card-text"> {{ (!is_null($books[$i]->author)) ? $books[$i]->author->name : '' }} </p>
            </div>
            <div class="panel-footer">
              <a href="{{ url('/book-detail').'/'.$books[$i]->id }}" class="btn btn-primary">Detail</a>
              @role(['member', 'admin'])
                @if(!is_null($books[$i]->pdf_file) && file_exists(public_path().'/pdf/'.$books[$i]->pdf_file))
                  <a href="{{ url('/read').'/'.$books[$i]->id }}" class="btn btn-primary">Baca</a>
                @else
                  <a href="#" class="text-muted pull-right non-pdf"> <i>  PDF tidak tersedia</i> </a>
                @endif
              
              @endrole
            </div>
          </div>
        </div>
        @php
          $rowCount++;
          if($rowCount % $numberOfColumns == 0) echo '</div><div class="row">';
        @endphp
      @endfor
    @else
      <div class="col-lg-12">
        <h5 class="text-muted"><i> Belum ada data buku.</i></h5>
      </div>
    @endif
    </div>

    {{-- Book Pagination --}}
    <div class="row">
      <div class="col-lg-12">
        <center>{{ $books->links('vendor.pagination.default') }}</center>
      </div>
    </div>

    
    {{-- 
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h2 class="panel-title">Daftar Buku</h2>
          </div>

          <div class="panel-body">
            {!! $html->table(['class'=>'table-striped']) !!}
          </div>
        </div>
      </div>
    </div>
    --}}

  </div>
@endsection

@section('scripts')
  <script type="text/javascript">

  </script>
@endsection

