@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">

      @include('layouts._admin-menu')

      <div class="col-lg-10 col-md-10">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h2 class="panel-title">Dashboard</h2>
          </div>

          <div class="panel-body">
              Selamat datang di Administrasi {{ config('app.name', 'Laravel') }}. Silahkan pilih menu administrasi yang diinginkan.
              <hr>
              <h4>Statistik Kategori Buku</h4>
              <canvas id="chartPenulis" width="400" height="150"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
    <script src="/js/Chart.min.js"></script>
    <script>
    var data = {
        labels: {!! json_encode($categories) !!},
        datasets: [{
            label: 'Jumlah buku',
            data: {!! json_encode($books) !!},
            backgroundColor: "rgba(151,187,205,0.5)",
            borderColor: "rgba(151,187,205,0.8)",
        }]
    };

    var options = {
        scales: {
            /*yAxes: [{
                ticks: {
                    beginAtZero:false,
                    stepSize: 100
                }
            }]*/
        }
    };

    var ctx = document.getElementById("chartPenulis").getContext("2d");

    var authorChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });
    </script>
@endsection
