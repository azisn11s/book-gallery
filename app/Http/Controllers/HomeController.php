<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laratrust\LaratrustFacade as Laratrust;
use Illuminate\Support\Facades\Auth;
use App\Author;
use App\Category;
use App\Book;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Laratrust::hasRole('admin')) return $this->adminDashboard();
        if (Laratrust::hasRole('member')) return $this->memberDashboard();

        return view('home');
    }

    protected function adminDashboard()
    {
        $categories = [];
        $books = [];
        foreach (Category::all() as $category) {
            array_push($categories, $category->name);
            array_push($books, $category->books->count());
        }

        return view('dashboard.admin', compact('categories', 'books'));
    }

    protected function memberDashboard()
    {
        $books = Book::orderby('created_at','desc')->paginate(8);

        return view('guest.index')->with(compact('books'));

        /*$borrowLogs = Auth::user()->borrowLogs()->borrowed()->get();
        return view('dashboard.member', compact('borrowLogs'));*/
    }


}
