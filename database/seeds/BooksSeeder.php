<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Author;
use App\Book;
use App\BorrowLog;
use App\Category;
use App\Publisher;
use Faker\Factory as Faker;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat contoh data Category;
        $category1 = new Category();
        $category1->name = 'Dongeng & Anak';
        $category1->save();

        $category2 = new Category();
        $category2->name = 'Desain Interior';
        $category2->save();

        // Contoh Data Penerbit
        $publisher1 = new Publisher();
        $publisher1->name = 'PT. Airlangga TBK.';
        $publisher1->save();

        $publisher2 = new Publisher();
        $publisher2->name = 'PT. Sinar Mulia';
        $publisher2->save();

        // Sample penulis
        $author1 = Author::create(['name'=>'Mohammad Fauzil Adhim']);
        $author2 = Author::create(['name'=>'Salim A. Fillah']);
        $author3 = Author::create(['name'=>'Aam Amiruddin']);

        // Faker
        $faker = Faker::create();

        // Count Master Data
        $category_number = Category::count();
        $publisher_number = Publisher::count();
        $author_number = Author::count();

        // Image
        // $imageUrl = $faker->imageUrl(300, 400);
        // $image = $faker->image();
        // $file_name = explode('/', $imageUrl);

        // Sample buku
        /*for ($i=0; $i < 100; $i++) { 
            
            $imageUrl = $faker->imageUrl(300, 400);
            $file_name = explode('/', $imageUrl);

            Book::create([
                'title'=> $faker->sentence(3),
                'author_id'=> rand(1, $author_number),
                'category_id'=> rand(1, $category_number),
                'publisher_id'=> rand(1, $publisher_number),
                'cover'=> end($file_name),
                'isbn'=> $faker->isbn13(),
                'publish_place'=> $faker->city(),
                'publish_date'=> $faker->date('Y-m-d'),
                'total_page'=> rand(50, 200),
                'seen_count'=> rand(12, 50),
                'description'=> $faker->paragraph(7),
            ]);
        }*/

        /*$book1 = Book::create([
            'title'=>'Kupinang Engkau dengan Hamdalah',
            'author_id'=>$author1->id,
            'category_id'=>$category1->id,
            'publisher_id'=>$publisher1->id
        ]);
        $book2 = Book::create([
            'title'=>'Jalan Cinta Para Pejuang',
            'author_id'=>$author2->id,
            'category_id'=>$category2->id,
            'publisher_id'=>$publisher2->id
        ]);
        $book3 = Book::create([
            'title'=>'Membingkai Surga dalam Rumah Tangga',
            'author_id'=>$author3->id,
            'category_id'=>$category1->id,
            'publisher_id'=>$publisher1->id
        ]);*/
       

        // Sample peminjaman buku
        /*$member = User::where('email', 'member@gmail.com')->first();
        BorrowLog::create(['user_id' => $member->id, 'book_id'=>$book1->id, 'is_returned' => 0]);
        BorrowLog::create(['user_id' => $member->id, 'book_id'=>$book2->id, 'is_returned' => 0]);
        BorrowLog::create(['user_id' => $member->id, 'book_id'=>$book3->id, 'is_returned' => 1]);*/
    }
}
